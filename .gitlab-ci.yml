image: ruby:2.4

stages:
  - build
  - test
  - deploy

before_script:
  - ruby -v
  - bundle install --jobs 4 --path vendor

variables:
  BRANCH_CE: 'master'
  BRANCH_EE: 'master'
  BRANCH_OMNIBUS: 'master'
  BRANCH_RUNNER: 'master'

.build_base: &build_base
  stage: build
  script:
    - rake setup_git default
    - nanoc compile -VV
  artifacts:
    paths:
      - public
    expire_in: 1w
  cache:
    key: "$CI_BUILD_NAME/$CI_BUILD_REF_NAME"
    paths:
      - tmp/
      - vendor/ruby
  tags:
    - docker

# Compile on master branch
compile_master:
  <<: *build_base
  variables:
    NANOC_ENV: 'production'
  only:
    - master

# Compile on all branches except master
compile_branch:
  <<: *build_base
  only:
    - branches@gitlab-com/gitlab-docs
  except:
    - master

# Check for 404s in internal links
internal_links:
  stage: test
  script:
    - nanoc check internal_links
  allow_failure: true
  cache:
    key: "$CI_BUILD_NAME/$CI_BUILD_REF_NAME"
    paths:
      - vendor/ruby
  tags:
    - docker
  # Skip this job when it's invoked by a cross project pipeline. That will speed
  # up the pipeline when a docs preview is triggered. We already check for
  # internal links in every MR anyway. For more info:
  # https://docs.gitlab.com/ee/development/writing_documentation.html#previewing-the-changes-live
  except:
    - pipelines

# SCSS linting
scss_lint:
  stage: test
  script:
    - bundle exec scss-lint
  cache:
    key: "$CI_BUILD_NAME/$CI_BUILD_REF_NAME"
    paths:
      - vendor/ruby
  tags:
    - docker
  # Skip this job when it's invoked by a cross project pipeline. That will speed
  # up the pipeline when a docs preview is triggered. The triggered pipeline is
  # always a branch off master which should be green anyway. For more info:
  # https://docs.gitlab.com/ee/development/writing_documentation.html#previewing-the-changes-live
  except:
    - pipelines

# Deploy the Review App
review:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  dependencies:
    - compile_branch
  before_script: []
  cache: {}
  script:
    - rsync -av --delete public /srv/nginx/pages/$CI_BUILD_REF_SLUG
  environment:
    name: review/$CI_BUILD_REF_SLUG
    url: http://$CI_BUILD_REF_SLUG.$APPS_DOMAIN
    on_stop: review_stop
  only:
    - branches@gitlab-com/gitlab-docs
  except:
    - master
  tags:
    - nginx
    - review-apps

# Stop the Review App
review_stop:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  dependencies: []
  artifacts: {}
  before_script: []
  cache: {}
  script:
    - rm -rf public /srv/nginx/pages/$CI_BUILD_REF_SLUG
  when: manual
  environment:
    name: review/$CI_BUILD_REF_SLUG
    action: stop
  only:
    - branches@gitlab-com/gitlab-docs
  except:
    - master
  tags:
    - nginx
    - review-apps

# Deploy to Pages
pages:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  before_script: []
  cache: {}
  environment:
    name: production
    url: https://docs.gitlab.com
  script:
    # Symlink all README.html to index.html
    - for i in `find public -name README.html`; do ln -sf README.html $(dirname $i)/index.html; done
  artifacts:
    paths:
    - public
    expire_in: 1h
  only:
    - master@gitlab-com/gitlab-docs
  tags:
    - docker
  dependencies:
    - compile_master
